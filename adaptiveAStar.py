from repeatedAStar import RepeatedAStar


class AdaptiveAStar(RepeatedAStar):

    '''Adaptive A*'''

    def __init__(self, grid, start, target):
        RepeatedAStar.__init__(self, grid, start, target, is_adaptive=True)
