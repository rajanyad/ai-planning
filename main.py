from grid_generation import GridGeneration
from visualize import Visualize
from repeatedFAStar import RepeatedFAStar
from repeatedBAStar import RepeatedBAStar
from adaptiveAStar import AdaptiveAStar
from constants import *


def _plan_and_display(planning, p_grid, text, p_visualize):
    is_reachable = planning.is_reachable()

    if is_reachable:
        print "Target cell reached!"
    else:
        print "Target cell not reachable!"

    print text + str(planning.get_expansion_count())

    p_visualize.show(p_grid)


def _clean_grid(c_grid):
    for row in range(ROW_COUNTS):
        for col in range(COL_COUNTS):
            if c_grid[row][col] == '2':
                c_grid[row][col] = '0'
            if (row, col) == start or (row, col) == target:
                c_grid[row][col] = '3'


if __name__ == '__main__':

    grids = []
    grid_generator = GridGeneration()

    print "####### Generating 50 gridworlds of size 101X101 ###########"
    for loop in range(50):
        grids.append(grid_generator.get_grid())

    visualize = Visualize()
    loop = 1
    for grid in grids:
        print "\n\n####### Processing Grid #" + str(loop) + " #######"

        visualize.show(grid)

        start = visualize.get_start()
        target = visualize.get_target()

        print "####### Running Repeated Forward A* ########"

        print "Case: Break tie with greater g-value"

        fwd_planning_g = RepeatedFAStar(grid, start, target, 'G')
        _plan_and_display(fwd_planning_g, grid, "No.of cells expanded in Repeated Forward A* (greater g-value tie breaker): ", visualize)

        print "Case: Break tie with smaller g-value"

        _clean_grid(grid)

        fwd_planning_s = RepeatedFAStar(grid, start, target, 'S')
        _plan_and_display(fwd_planning_s, grid, "No.of cells expanded in Repeated Forward A* (smaller g-value tie breaker): ", visualize)

        print "####### Running Repeated Backward A* ########"

        _clean_grid(grid)

        bwd_planning = RepeatedBAStar(grid, start, target)
        _plan_and_display(bwd_planning, grid, "No.of cells expanded in Repeated Backward A*: ", visualize)

        print "####### Running Adaptive A* ########"

        _clean_grid(grid)

        adap_planning = AdaptiveAStar(grid, start, target)
        _plan_and_display(adap_planning, grid, "No.of cells expanded in Adaptive A*: ", visualize)

        loop = loop + 1

