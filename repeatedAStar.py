from constants import *
from heap import Heap


class RepeatedAStar(object):
    def __init__(self, grid, start, target, is_adaptive=False, is_backward=False, break_tie='G'):
        self.grid = grid
        self.c = len(self.grid)*len(self.grid)

        self.start = start
        self.target = target
        self.is_adaptive = is_adaptive
        self.is_backward = is_backward
        self.break_tie = break_tie

        self.expansion_count = 0

        self.heap = Heap()

    def _traversal_initialize(self):
        if self.is_adaptive:
            self.closed = []

        self.open = []
        self.counter = 0

        self.g = {}
        self.f = {}
        self.h = {}
        self.t = {}

        self.parent = {}

        self.blocked_cells = {}

        self.search = []
        for row in range(ROW_COUNTS):
            row_list = []
            for col in range(COL_COUNTS):
                row_list.append(0)
            self.search.append(row_list)

        #self.visualize = Visualize()

    @staticmethod
    def _get_manhattan_dist(u, v):
        return abs(v[0]-u[0]) + abs(v[1]-u[1])

    @staticmethod
    def _neighbors(u):
        ux = u[0]
        uy = u[1]

        neighs_list = []
        if (ux - 1) > -1:
            neighs_list.append((ux - 1, uy))

        if (ux + 1) < ROW_COUNTS:
            neighs_list.append((ux + 1, uy))

        if (uy - 1) > -1:
            neighs_list.append((ux, uy - 1))

        if (uy + 1) < COL_COUNTS:
            neighs_list.append((ux, uy + 1))

        return neighs_list

    # def _clean_expanded_in_grid(self):
    #     for row in range(ROW_COUNTS):
    #         for col in range(COL_COUNTS):
    #             if self.grid[row][col] == '4':
    #                 self.grid[row][col] = 0

    def _compute_path(self, target):

        min_f, min_t, u = self.heap.heap_min(self.open)
        while self.g[target] > min_f:
            self.heap.heap_pop(self.open)
            if self.is_adaptive:
                self.closed.append(u)

            # if self.grid[u[0]][u[1]] == 0:
            #     self.grid[u[0]][u[1]] = 4

            self.expansion_count = self.expansion_count + 1

            is_all_children_blocked = True
            for v in self._neighbors(u):
                if v not in self.blocked_cells.keys():
                    is_all_children_blocked = False
                    if self.search[v[0]][v[1]] < self.counter:
                        self.g[v] = INFINITE
                        self.search[v[0]][v[1]] = self.counter
                    if self.g[v] > (self.g[u] + 1):
                        self.g[v] = self.g[u] + 1
                        self.parent[v] = u
                        if v in self.f.keys():
                            if (self.f[v], self.t[v], v) in self.open:
                                self.open.remove((self.f[v], self.t[v], v))

                        if self.is_adaptive:
                            if v not in self.h.keys():
                                self.h[v] = self._get_manhattan_dist(v, target)
                        else:
                            self.h[v] = self._get_manhattan_dist(v, target)

                        self.f[v] = self.g[v] + self.h[v]
                        if self.break_tie == 'G':
                            self.t[v] = self.c * self.f[v] - self.g[v]
                        else:
                            self.t[v] = self.g[v]
                        self.heap.heap_push(self.open, (self.f[v], self.t[v], v))

            if is_all_children_blocked:
                self.blocked_cells[u] = 1

            if len(self.open) == 0:
                break
            min_f, min_t, u = self.heap.heap_min(self.open)

        # self.visualize.show(self.grid)
        # self._clean_expanded_in_grid()

    def _update_blocked_neighs(self, u):
        for v in self._neighbors(u):
            if self.grid[v[0]][v[1]] == '1':
                self.blocked_cells[v] = 1

    def _update_adaptive_heuristic(self, closed, target):
        while len(closed) > 0:
            v = closed.pop()
            self.h[v] = self.g[target] - self.g[v]

    def _get_presumed_path(self, start, target):

        self.g[start] = 0
        self.search[start[0]][start[1]] = self.counter

        self.g[target] = INFINITE
        self.search[target[0]][target[1]] = self.counter

        if self.is_adaptive:
            if start not in self.h.keys():
                self.h[start] = self._get_manhattan_dist(start, target)
        else:
            self.h[start] = self._get_manhattan_dist(start, target)
        self.f[start] = self.g[start] + self.h[start]

        if self.break_tie == 'G':
            self.t[start] = self.c * self.f[start] - self.g[start]
        else:
            self.t[start] = self.g[start]

        if self.is_adaptive:
            self.closed = []
        self.open = []

        self.open.append((self.f[start], self.t[start], start))
        self.heap.build_heap(self.open)

        self._compute_path(target)

        if len(self.open) == 0:
            return []

        cur_parent = target
        path = []
        while cur_parent != start:
            if self.is_backward:
                path.insert(0, cur_parent)
            else:
                path.append(cur_parent)
            cur_parent = self.parent[cur_parent]

        if self.is_backward:
            path.insert(0, start)

        return path

    def get_expansion_count(self):
        return self.expansion_count

    def is_reachable(self):
        self._traversal_initialize()

        start = self.start
        while start != self.target:
            self.grid[start[0]][start[1]] = '2'

            self.counter = self.counter + 1

            self._update_blocked_neighs(start)

            if self.is_backward:
                path = self._get_presumed_path(self.target, start)
            else:
                path = self._get_presumed_path(start, self.target)

            if len(path) == 0:
                return 0

            while len(path) != 0:
                (cur_cell_x, cur_cell_y) = path[-1]
                if self.grid[cur_cell_x][cur_cell_y] == '1' or (cur_cell_x, cur_cell_y) in self.blocked_cells.keys():
                    break
                start = path.pop()

            if self.is_adaptive:
                if self.is_backward:
                    self._update_adaptive_heuristic(self.closed, start)
                else:
                    self._update_adaptive_heuristic(self.closed, self.target)

        self.grid[self.target[0]][self.target[1]] = '2'
        return 1