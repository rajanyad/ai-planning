import pygame
from constants import *


class Visualize(object):

    def __init__(self):
        self.start = (0, 0)
        self.target = (ROW_COUNTS-1, COL_COUNTS-1)

    def show(self, grid):
        # Initialize pygame
        pygame.init()

        # Set the HEIGHT and WIDTH of the screen
        WINDOW_SIZE = [(WIDTH+MARGIN)*COL_COUNTS, (HEIGHT+MARGIN)*ROW_COUNTS]
        screen = pygame.display.set_mode(WINDOW_SIZE)

        # Set title of screen
        pygame.display.set_caption("Grid")

        # Loop until the user clicks the close button.
        done = False

        # Used to manage how fast the screen updates
        clock = pygame.time.Clock()

        is_start_chosen = False

        while not done:
            for event in pygame.event.get():  # User did something
                if event.type == pygame.QUIT:  # If user clicked close
                    done = True  # Flag that we are done so we exit this loop
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    # User clicks the mouse. Get the position
                    pos = pygame.mouse.get_pos()
                    # Change the x/y screen coordinates to grid coordinates
                    column = pos[0] // (WIDTH + MARGIN)
                    row = pos[1] // (HEIGHT + MARGIN)
                    # Set that location to one
                    grid[row][column] = '3'

                    if not is_start_chosen:
                        self.start = (row, column)
                        is_start_chosen = True
                    else:
                        self.target = (row, column)

                    #print("Click ", pos, "Grid coordinates: ", row, column)

            # Set the screen background
            screen.fill(RED)

            # Draw the grid
            for row in range(ROW_COUNTS):
                for column in range(COL_COUNTS):
                    color = WHITE
                    if grid[row][column] == '1':
                        color = BLACK
                    elif grid[row][column] == '2':
                        color = GREEN
                    elif grid[row][column] == '3':
                        color = RED
                    elif grid[row][column] == '4':
                        color = GRAY
                    pygame.draw.rect(screen,
                                color,
                                [(MARGIN + WIDTH) * column + MARGIN,
                                (MARGIN + HEIGHT) * row + MARGIN,
                                WIDTH,
                                HEIGHT])

            # Limit to 60 frames per second
            clock.tick(60)

            # Go ahead and update the screen with what we've drawn.
            pygame.display.flip()

        # Be IDLE friendly. If you forget this line, the program will 'hang' on exit.
        pygame.quit()

    def get_start(self):
        return self.start

    def get_target(self):
        return self.target