from constants import *
import random


class Cell:
    def __init__(self, visited=False, parent=(-1,-1), blocked=False):
        self.visited = visited
        self.parent = parent
        self.blocked = blocked


class GridGeneration:

    def __init__(self):
        print "Grid Generator!"

    @staticmethod
    def _should_block():
        if random.random() <= 0.3:  #With 30% probability, block a cell
            return True
        else:
            return False

    @staticmethod
    def _neighbours(x, y):
        adj_list= []
        if (x-1) > -1:
            adj_list.append((x-1, y))
        if (x+1) < ROW_COUNTS:
            adj_list.append((x+1, y))
        if (y-1) > -1:
            adj_list.append((x, y-1))
        if (y+1) < COL_COUNTS:
            adj_list.append((x, y+1))

        return adj_list

    def _dfs_visit(self, grid, start_x, start_y):

        grid[start_x][start_y].visited = True
        stack = [(start_x, start_y)]
        while len(stack) != 0:
            (ux, uy) = stack.pop()
            for (vx, vy) in self._neighbours(ux, uy):
                if not grid[vx][vy].visited:
                    grid[vx][vy].visited = True
                    if self._should_block():
                        grid[vx][vy].blocked = True
                    else:
                        grid[vx][vy].parent = (ux, uy)
                        stack.append((vx, vy))

    def _dfs(self, grid):
        for row in range(ROW_COUNTS):
            for col in range(COL_COUNTS):
                if not grid[row][col].visited:
                    self._dfs_visit(grid, row, col)

    def get_grid(self):

        dfs_grid = []

        for row in range(ROW_COUNTS):
            row = []
            for col in range(COL_COUNTS):
                row.append(Cell())
            dfs_grid.append(row)

        self._dfs(dfs_grid)

        grid = []
        for row in range(ROW_COUNTS):
            row_list = []
            for col in range(COL_COUNTS):
                if dfs_grid[row][col].blocked:
                    row_list.append('1')
                else:
                    row_list.append('0')
            grid.append(row_list)

        return grid
