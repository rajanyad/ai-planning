ROW_COUNTS = 101
COL_COUNTS = 101
INFINITE = 1000000

# define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
GRAY = (169, 169, 169)

# This sets the WIDTH and HEIGHT of each grid location
WIDTH = 5
HEIGHT = 5

# This sets the margin between each cell
MARGIN = 1