from repeatedAStar import RepeatedAStar


class RepeatedBAStar(RepeatedAStar):

    '''Repeated Backward A*'''

    def __init__(self, grid, start, target):
        RepeatedAStar.__init__(self, grid, start, target, is_backward=True)
