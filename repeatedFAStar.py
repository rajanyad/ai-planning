from repeatedAStar import RepeatedAStar


class RepeatedFAStar(RepeatedAStar):

    '''Repeated Forward A*'''

    def __init__(self, grid, start, target, break_tie):
        RepeatedAStar.__init__(self, grid, start, target, break_tie=break_tie)
