
class Heap(object):
    def __init__(self):
        self.heap_size = 0

    @staticmethod
    def _parent(i):
        if i % 2 == 0:
            return i/2 - 1
        else:
            return i/2

    @staticmethod
    def _left(i):
        return 2*i + 1

    @staticmethod
    def _right(i):
        return 2*i + 2

    def heapify(self, array, i):
        l_i = self._left(i)
        r_i = self._right(i)

        smallest = i
        if l_i < self.heap_size:
            if type(array[l_i]) is tuple:
                val_l = array[l_i][0]
            else:
                val_l = array[l_i]
            if type(array[i]) is tuple:
                val_i = array[i][0]
            else:
                val_i = array[i]

            if val_l < val_i:
                smallest = l_i
            elif val_l == val_i:
                if len(array[0]) > 2:
                    if array[l_i][1] < array[i][1]:
                        smallest = l_i

        if r_i < self.heap_size:
            if type(array[r_i]) is tuple:
                val_r = array[r_i][0]
            else:
                val_r = array[r_i]
            if type(array[smallest]) is tuple:
                val_s = array[smallest][0]
            else:
                val_s = array[smallest]

            if val_r < val_s:
                smallest = r_i
            elif val_r == val_s:
                if len(array[0]) > 2:
                    if array[r_i][1] < array[smallest][1]:
                        smallest = r_i

        if smallest != i:
            swap = array[smallest]
            array[smallest] = array[i]
            array[i] = swap

            self.heapify(array, smallest)

    def build_heap(self, array):
        self.heap_size = len(array)
        index = len(array)/2 - 1
        while index >= 0:
            self.heapify(array, index)
            index = index - 1

    def heap_min(self, array):
        return array[0]

    def heap_pop(self, array):
        if self.heap_size < 1:
            print "heap underflow!"
            return
        min = array[0]
        array[0] = array[self.heap_size-1]
        self.heap_size = self.heap_size - 1
        array.pop()
        self.heapify(array, 0)
        return min

    def _heap_decrease_key(self, array, i):
        p_i = self._parent(i)
        while i > 0:
            if type(array[0]) is tuple:
                if len(array[0]) <= 2:
                    if array[p_i][0] <= array[i][0]:
                        break
                else:
                    if array[p_i][0] < array[i][0]:
                        break
                    if array[p_i][0] == array[i][0]:
                        if array[p_i][1] <= array[i][1]:
                            break
            else:
                if array[p_i] <= array[i]:
                    break
            swap = array[p_i]
            array[p_i] = array[i]
            array[i] = swap
            i = p_i
            p_i = self._parent(i)

    def heap_push(self, array, key):
        array.append(key)
        self.heap_size = len(array)
        self._heap_decrease_key(array, self.heap_size - 1)


## Testing
if __name__ == '__main__':
    heap = Heap()
    A = [(4, 1, "abc"), (1,2,"er"), (6,3, "rt"), (1, 1, "rtg"), (4, 5, "pu"), (4, 2, "we")]
    heap.build_heap(A)
    heap.heap_push(A, (1,3,"erd"))
    for i in range(6):
        print heap.heap_min(A)
        print heap.heap_pop(A)
